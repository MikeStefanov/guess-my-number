"use strict";

let secretNumber = Math.trunc(Math.random() * 20) + 1;
let score = 20;
let highScore = 0;

const displayMessage = function (message) {
  document.querySelector(".message").textContent = message;
};

const displayScore = function (score) {
  document.querySelector(".score").textContent = score;
};

const displayNumber = function (number) {
  document.querySelector(".number").textContent = number;
};

const bodyStyle = function (style) {
  document.querySelector("body").style.backgroundColor = style;
};

const numberStyle = function (style) {
  document.querySelector(".number").style.width = style;
};

document.querySelector(".check").addEventListener("click", function () {
  const guess = Number(document.querySelector(".guess").value);

  if (!guess) {
    displayMessage("⛔ No Number");
  } else if (secretNumber === guess) {
    displayMessage("Correct Number 🎉");
    displayNumber(secretNumber);

    // High Score
    if (score > highScore) {
      highScore = score;
      document.querySelector(".highscore").textContent = highScore;
    }

    // change the CSS Style
    bodyStyle("#60b347");
    numberStyle("30rem");
  } else if (secretNumber !== guess) {
    if (score > 1) {
      displayMessage(guess > secretNumber ? "📈 Too high!" : "Too Low 📉");
      score--;
      displayScore(score);
    } else {
      displayMessage("💥 You lost the game!");
      displayScore(0);
    }
  }
});

document.querySelector(".again").addEventListener("click", function () {
  bodyStyle("#222");
  numberStyle("15rem");
  displayNumber("?");
  displayMessage("Start guessing...");
  displayScore(20);
  document.querySelector(".guess").value = "";
  secretNumber = Math.trunc(Math.random() * 20) + 1;
  score = 20;
});
